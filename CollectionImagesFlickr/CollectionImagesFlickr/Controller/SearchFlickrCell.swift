//
//  SearchFlickrCell.swift
//  CollectionImagesFlickr
//
//  Created by MuhaMaD on 4/3/1401 AP.
//

import UIKit

class SearchFlickrCell: UICollectionViewCell {

    static let identifier = "CollectionCell"
    
    @IBOutlet weak var image: UIImageView!
    @IBOutlet weak var label: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        contentView.layer.borderColor = UIColor.systemGray.cgColor
        contentView.layer.borderWidth = 0.5
        contentView.layer.cornerRadius = 5
        
        image.contentMode = .scaleAspectFill
        image.layer.cornerRadius = 5
        
        label.backgroundColor = .systemGray6
        label.layoutIfNeeded()
        label.isHidden = true
    }

    static func nib() -> UINib {
        return UINib(nibName: "SearchFlickrCell", bundle: nil)
    }
    
    func config(imageURL: String , title: String) {
        image.downloadImage(from: imageURL) { response in
            DispatchQueue.main.async {
                self.image.image = response.image
            }
        }
        label.text = title
    }
    
    
    func flipCell() {
            let flipSide: UIView.AnimationOptions = label.isHidden ? .transitionFlipFromLeft : .transitionFlipFromRight
            UIView.transition(with: self.contentView, duration: 0.3, options: flipSide, animations: { [weak self] in
                self?.image.isHidden.toggle()
                self?.label.isHidden.toggle()
            }, completion: nil)
        }
    
}
