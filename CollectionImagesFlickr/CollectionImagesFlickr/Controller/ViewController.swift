//
//  ViewController.swift
//  CollectionImagesFlickr
//
//  Created by MuhaMaD on 30/2/1401 AP.
//

import UIKit

class ViewController: UIViewController {
    
    @IBOutlet weak var searchBarField: UISearchBar!
    @IBOutlet weak var gridCollectionView: UICollectionView!
    
    var searchModel = SearchModel.instance
    
    private let spacingForCell: CGFloat = 8.0
    
    private var selectedStatHeaders = Set<Int>()
    
    private let searchFlickr = SearchFlickrController()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        firstLoadingInit()
        setupViews()
        doneKeyboardUI()
    }
    
    func firstLoadingInit() {
        searchBarField.text = "cat"
        
        searchFlickr.searchRequest(baseURL: .flickr, tag: "cat") { response in
            self.searchModel = response
            self.gridCollectionView.reloadData()
        }
    }
    
    func doneKeyboardUI() {
        //init toolbar
        let toolbar:UIToolbar = UIToolbar(frame: CGRect(x: 0, y: 0,  width: self.view.frame.size.width, height: 30))
        //create left side empty space so that done button set on right side
        let flexSpace = UIBarButtonItem(barButtonSystemItem:    .flexibleSpace, target: nil, action: nil)
        let doneBtn: UIBarButtonItem = UIBarButtonItem(title: "Done", style: .done, target: self, action: #selector(doneButtonAction))
        toolbar.setItems([flexSpace, doneBtn], animated: false)
        toolbar.sizeToFit()
        //setting toolbar as inputAccessoryView
        self.searchBarField.inputAccessoryView = toolbar
    }
    @objc func doneButtonAction() {
        self.view.endEditing(true)
    }
    private func setupViews() {
        gridCollectionView
            .register(SearchFlickrCell.nib(),
                      forCellWithReuseIdentifier: SearchFlickrCell.identifier)
        
        gridCollectionView.delegate = self
        gridCollectionView.dataSource = self
        
        let layout = UICollectionViewFlowLayout()
        layout.scrollDirection = .vertical
        
        /// your padding
        layout.minimumLineSpacing = spacingForCell
        /// half of the line-spacing
        layout.minimumInteritemSpacing = spacingForCell / 2
        
        gridCollectionView
            .setCollectionViewLayout(layout, animated: true)
        
        searchBarField.delegate = self
    }
}

//MARK: Collection Data Source
extension ViewController: UICollectionViewDataSource {
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return searchModel.photos?.photo?.count ?? 0
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        guard let cell = collectionView.dequeueReusableCell(withReuseIdentifier: SearchFlickrCell.identifier, for: indexPath) as? SearchFlickrCell else {
            return UICollectionViewCell()
        }
        
        DispatchQueue.global().async {
            let itemCell = self.searchModel.photos?.photo?[indexPath.item]
            DispatchQueue.main.async {
                cell.config(imageURL: itemCell?.url_m ?? "", title: itemCell?.title ?? "none")
            }
        }
        
        return cell
        
    }
    
}

//MARK: Collection Flow Layout
extension ViewController: UICollectionViewDelegateFlowLayout {
    
    func collectionView(_ collectionView: UICollectionView,
                        layout collectionViewLayout: UICollectionViewLayout,
                        insetForSectionAt section: Int) -> UIEdgeInsets {
        
        UIEdgeInsets(top: spacingForCell, left: spacingForCell, bottom: spacingForCell, right: spacingForCell)
    }
    
    
    func collectionView(_ collectionView: UICollectionView,
                        layout collectionViewLayout: UICollectionViewLayout,
                        sizeForItemAt indexPath: IndexPath) -> CGSize {
        /// instance for using minimumInteritemSpacing
        let lay = collectionViewLayout as! UICollectionViewFlowLayout
        /// grid to 3 cell for row
        let sizePerItem = (collectionView.frame.width / 3 - lay.minimumInteritemSpacing) - spacingForCell
        /// set width per item with edge insets
        return CGSize(width: sizePerItem, height: sizePerItem)
    }
}

//MARK: Collection Delegate
extension ViewController: UICollectionViewDelegate {
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        guard let cell = collectionView.cellForItem(at: indexPath) as? SearchFlickrCell else {
            return
        }
        cell.flipCell()
    }
}

extension ViewController: UISearchBarDelegate {
    
    func searchBarTextDidEndEditing(_ searchBar: UISearchBar) {
        let textInput = searchBar.text ?? ""
        
        searchFlickr.searchRequest(baseURL: .flickr, tag: textInput) { response in
            self.searchModel = response
            self.gridCollectionView.reloadData()
        }
        searchBar.endEditing(true)
    }
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        let textInput = searchBar.text ?? ""
        
        searchFlickr.searchRequest(baseURL: .flickr, tag: textInput) { response in
            self.searchModel = response
            self.gridCollectionView.reloadData()
        }
        searchBar.endEditing(true)
    }
}


