//
//  SearchFlickrController.swift
//  CollectionImagesFlickr
//
//  Created by MuhaMaD on 4/3/1401 AP.
//

import Foundation

struct SearchFlickrController {
    
    var searchModel = SearchModel.instance
    func searchRequest(baseURL: BaseURL, tag: String, model: @escaping (SearchModel) -> Void) {
        let s = APIService(baseURL: baseURL)
        SearchModel.tag = tag
        s.doCustomRequest(searchModel, method: .get) { response in
            
            switch response {
            case .success(let rawSearchModel):
                model(rawSearchModel)
            case .failure(let error):
                print(error)
                
            }
        }
    }
    
    
}
