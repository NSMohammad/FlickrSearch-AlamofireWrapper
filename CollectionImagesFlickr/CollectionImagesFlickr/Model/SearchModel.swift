//
//  SearchModel.swift
//  CollectionImagesFlickr
//
//  Created by MuhaMaD on 4/3/1401 AP.
//

import Foundation

struct SearchModel: Requestable {
    static var retriable: Bool {
        false
    }
    
    typealias ResponseType = SearchModel
    
    static var tag = ""
    static let apiKey = "fa99d5e37376a1c8f370a085e46c82ea"
    
    static var path: String {
        return "/services/rest/?method=flickr.photos.search&api_key=\(apiKey)&tags=\(tag)&extras=url_m&page=&format=json&nojsoncallback=1"
    }
    
    var photos: Photos?
    var stat: String?
    //
    static let instance = SearchModel()
    //    private init() {}
}
struct Photos: Codable {
    var page: Int?
    var pages: Int?
    var perpage: Int?
    var total: Int?
    var photo: [Photo]?
}
struct Photo: Codable {
    var id: String?
    var owner: String?
    var secret: String?
    var server: String?
    var farm: Int?
    var title: String?
    var ispublic: Int?
    var isfriend: Int?
    var isfamily: Int?
    var url_m: String?
    var height_m: Int?
    var width_m: Int?
}
